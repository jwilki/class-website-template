import React from 'react';

export default function Test() {
    return <>
        <h2>Une page qui n'est pas dans une section</h2>
        <p>
            Lorem ipsum dolor sit amet consectetur, adipisicing elit. Neque magni dolorum non recusandae odit, aliquid deleniti inventore aperiam fuga ea distinctio modi autem, placeat error, numquam perferendis temporibus dolores eligendi.
        </p>
    </>;
}