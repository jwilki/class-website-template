import React from 'react';

export default function Labo01() {
    return <>
        Labo01
        <ol>
            <li>Test 1</li>
            <li>
                Test 2
                <ol>
                    <li>Test 2.1</li>
                    <li>Test 2.2</li>
                    <li>Test 2.3</li>
                </ol>
            </li>
            <li>Test 3</li>
            <li>Test 4</li>
        </ol>
    </>;
}
