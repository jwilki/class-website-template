import React from 'react';
import InlineCode from '../component/InlineCode'
import CodeBlock from '../component/CodeBlock';
import ColoredBox from '../component/ColoredBox';
import WebExample from '../component/WebExample';
import DownloadBlock from '../component/DownloadBlock';
import Situation from '../component/Situation';
import Video from '../component/Video';

import file from '../resources/solution.zip'

const codeJS = 
`let allo = document.getElementById('test');
allo.innerHTML = 'asdfafqq';`;

const codePHP = 
`<ul>
<?php for($i = 0 ; i < 10 ; i++) { ?>
    <li>Test <?php echo $i; ?></li>
<?php } ?>
</ul>`;

const webExHTML = 
`<div class="container">
<div id="main" class="box">
    0
</div>
</div>`;

const webExCSS1 = 
`.container{
    position: relative;
}
    
.box{
    position: absolute;
    top: 0;
    right: 0;
}`;

const webExCSS2 = 
`.container{
    background-color: #CCC;
    height: 100px;
    position: relative;
}
    
.box{
    background-color: red;
    color: white;
    position: absolute;
    top: 0;
    right: 0;
}`;

const webExJS = 
`let div = document.getElementById('main');
let count = 0;
setInterval(() => {
    count++;
    div.innerHTML = count;
}, 1000)`;

const plain =
`Ceci est un
test de texte/code sans parser`;

export default function Instruction() {
    return <>
        Instruction <InlineCode>Ceci est un test <span style={ { fontWeight: "bold" } }>allo</span></InlineCode>

        <CodeBlock language="php"></CodeBlock>
        <CodeBlock language="js">{ codeJS }</CodeBlock>
        <CodeBlock language="php">{ codePHP }</CodeBlock>
        <CodeBlock language="plain">{ plain }</CodeBlock>

        <WebExample title="Test">
            <WebExample.Code type="html">{ webExHTML }</WebExample.Code>
            <WebExample.Code type="css">{ webExCSS1 }</WebExample.Code>
            <WebExample.Code type="css" display={ false }>{ webExCSS2 }</WebExample.Code>
            <WebExample.Code type="js">{ webExJS }</WebExample.Code>
        </WebExample>
        <ul>
            <li>Test 1</li>
            <li>
                Test 2
                <ul>
                    <li>Test 2.1</li>
                    <li>Test 2.2</li>
                    <li>Test 2.3</li>
                </ul>
            </li>
            <li>Test 3</li>
            <li>Test 4</li>
        </ul>
        <ol>
            <li>Test 1</li>
            <li>
                Test 2
                <ol>
                    <li>Test 2.1</li>
                    <li>Test 2.2</li>
                    <li>Test 2.3</li>
                </ol>
            </li>
            <li>Test 3</li>
            <li>Test 4</li>
        </ol>
        <ColoredBox heading="Yo!">
            Du texte ici blah, blah
        </ColoredBox>
        <Situation>
            Ceci est une boîte de situation.
        </Situation>
        <Video title="Titre de la vidéo" src="https://www.youtube.com/embed/HA3Ks8NLS-Y" />
        <p>
            <a href="https://google.com" target="_blank" rel="noopener noreferrer">Google</a>
        </p>
        <DownloadBlock>
            <DownloadBlock.File path={ file } name="solution.zip" />
            <DownloadBlock.File path={ file } name="AutreFichier.zip" />
            <DownloadBlock.File path={ file } name="test.jpeg" />
            <DownloadBlock.File path={ file } name="Fichier.txt" />
            <DownloadBlock.File path={ file } name="allo.js" />
            <DownloadBlock.File path={ file } name="aasdfasfdaf.zip" />
            <DownloadBlock.File path={ file } name="asfsadfasdfasf fg sdf sdfgsdfg dsfgdsg sdfs.js" />
            <DownloadBlock.File path={ file } name="zxcv.txt" />
            <DownloadBlock.File path={ file } name="r.txt" />
        </DownloadBlock>
    </>;
}
