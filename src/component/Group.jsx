import React from 'react';
import { Link } from "react-router-dom";
import { Helmet } from 'react-helmet-async';

import data from '../data';
import styles from './Group.module.css';

export default function Group(props) {
    return <>
        <Helmet>
            <title>{ props.group.title }</title>
            <meta name="description" content={ props.group.description } />
        </Helmet>

        <h1>{ props.group.title }</h1>
        { props.group.noSection ?
            <ul className={ styles.list }>
                { data.groups[props.group.id].pages.map((page) => 
                    <li key={ page.id }>
                        { page.component ? 
                            <Link to={ `/group/${ props.group.id }/${ page.id }` }>{ page.title }</Link> :
                            <a href={ page.url } target="_blank" rel="noopener noreferrer">{ page.title }</a>
                        }
                    </li>
                ) }
            </ul>
        :
            data.sections.filter((section) => section.groups[props.group.id] && !section.disabled).map((section) => 
                <React.Fragment key={ section.id }>
                    <h2>{ section.completeName } - { section.title }</h2>
                    <ul className={ styles.list }>
                        { section.groups[props.group.id].pages.map((page) => (
                            <li key={ page.id }>
                                { page.component ? 
                                    <Link to={ `/${ section.id }/${ page.id }` }>{ page.title }</Link> :
                                    <a href={ page.url } target="_blank" rel="noopener noreferrer">{ page.title }</a>
                                }
                            </li>
                        )) }
                    </ul>
                </React.Fragment>
            ) 
        }
    </>
}
