// Syntax highlighter imports for styles
import vs from 'react-syntax-highlighter/dist/esm/styles/prism/vs';
import vsdark from 'react-syntax-highlighter/dist/esm/styles/prism/vsc-dark-plus';

// Settings object
let settings = {
    naming: {
        title: 'Programmation Internet II',
        sections: 'Module'
    },
    themes: {
        default: 'light',
        properties: {
            light: {
                bgColor: '#fff',
                bgAccentColor: '#f78f20',
                bgAccentGradientColor: '#faa754',
                bgInteractiveColor: '#6d6a62',
                bgInteractiveAccentColor: '#fdddbc',
                bgHighlight: '#d6d6d6',
                bgHighlightAccent: '#fdddbc',
                borderInteractiveColor: '#a19d97',
                borderSeparatorColor: '#c3c3c3',
                textColor: '#222',
                textInvertedColor: '#fff',
                textAsideColor: '#222',
                textSwitcherColor: '#f78f20',
                syntaxHighlightStyle: 'vs'
            },
            dark: {
                bgColor: '#36352e',
                bgAccentColor: '#eb5539',
                bgAccentGradientColor: '#f3734e',
                bgHighlight: '#87867c',
                bgHighlightAccent: '#eb5539',
                bgInteractiveColor: '#f9ae90',
                bgInteractiveAccentColor: '#f9ccc4',
                borderInteractiveColor: '#f3734e',
                borderSeparatorColor: '#c3c3c3',
                textColor: '#fff',
                textInvertedColor: '#fff',
                textAsideColor: '#f9ae90',
                textSwitcherColor: '#f9ae90',
                syntaxHighlightStyle: 'vsdark'
            }
        }
    },
    syntaxHiglight: {
        languages: {
            html: { name: 'HTML', parser: 'markup' },
            css: { name: 'CSS', parser: 'css' },
            js: { name: 'Javascript', parser: 'javascript' },
            php: { name: 'PHP', parser: 'php' },
            plain: { name: 'Plain Text' }
        },
        themes: {
            vs: vs,
            vsdark: vsdark
        }
    }
}

export default settings;
export let naming = settings.naming;
export let themes = settings.themes;
export let syntaxHiglight = settings.syntaxHiglight;